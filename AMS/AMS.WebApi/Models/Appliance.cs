﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AMS.WebApi.Models
{
    public class TenantApplianceModel
    {
        public int TenantId { get; set; }
        public List<Appliance> Appliances { get; set; }
    }

    public class Appliance
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public int StatusId { get; set; }
    }
}