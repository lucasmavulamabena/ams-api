﻿using AMS.WebApi.EF;
using AMS.WebApi.Models;
using Swashbuckle.Swagger.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace AMS.WebApi.Controllers
{
    public class ApplianceManagementController : ApiController
    {
        private ApplianceManagementEntities db = new ApplianceManagementEntities();
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        [SwaggerOperation("Create")]
        [SwaggerResponse(HttpStatusCode.Created)]
        public void Post(TenantApplianceModel ta)
        {
            foreach (var tap in ta.Appliances)
            {
                var tenantapp = db.TenantAppliances.Single(t => t.TenantId == ta.TenantId
                                                                && t.ApplianceId == tap.Id);

                if (tenantapp.ApplianceStatus.Any())
                {
                    tenantapp.ApplianceStatus.Single().StatusID = tap.StatusId;
                }
            }

            db.SaveChanges();
        }

        // PUT api/values/5
        [SwaggerOperation("Update")]
        [SwaggerResponse(HttpStatusCode.OK)]
        [SwaggerResponse(HttpStatusCode.NotFound)]
        public void Put(int id, [FromBody]string value)
        {
        }     

        // DELETE api/values/5
        [SwaggerOperation("Delete")]
        [SwaggerResponse(HttpStatusCode.OK)]
        [SwaggerResponse(HttpStatusCode.NotFound)]
        public void Delete(int id)
        {
        }
    }
}
